### Remindr

Remindr automatically sends reminders about last blog entries on social networks, like the [Mastodon](https://joinmastodon.org) or ([Twitter](https://twitter.com).
For the full documentation, [read it online](https://remindr.readthedocs.org/en/latest/).

If you would like, you can [support the development of this project on Liberapay](https://liberapay.com/carlchenet/).
Alternatively you can donate cryptocurrencies:

- BTC: 1MaFaUbmJcTVN9wsm5bzqFyq6zWu434X3A
- XMR: 43GGv8KzVhxehv832FWPTF7FSVuWjuBarFd17QP163uxMaFyoqwmDf1aiRtS5jWgCiRsi73yqedNJJ6V1La2joznKHGAhDi

### Quick Install

* Install Remindr from PyPI

        # pip3 install remindr

* Install Remindr from sources
  *(see the installation guide for full details)
  [Installation Guide](http://remindr.readthedocs.org/en/latest/install.html)*


        # tar zxvf remindr-0.1.tar.gz
        # cd remindr
        # python3 setup.py install
        # # or
        # python3 setup.py install --install-scripts=/usr/bin

### Create the authorization for the Remindr app

* Just launch the following command::

        $ register_remindr_app

### Use Remindr

* Create or modify remindr.ini file in order to configure remindr:

        [mastodon]
        instance_url=https://mastodon.social
        user_credentials=remindr_usercred.txt
        client_credentials=remindr_clientcred.txt
        ; Default visibility is public, but you can override it:
        ; toot_visibility=unlisted

        [entrylist]
        path_to_list=/etc/remindr/list.txt

        [prefix]
        en_prefix=My Last Blog Post:
        fr_prefix=Mon dernier billet de blog :

* Launch Remindr

        $ remindr -c /path/to/remindr.ini

### Authors

* Carl Chenet <chaica@ohmytux.com>

### License

This software comes under the terms of the GPLv3+.

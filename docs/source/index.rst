Documentation for the Remindr project
=====================================

Remindr automatically sends toots/tweets from a user-defined list to remind people about the last blog entries using the social networks. You'll find below anything you need to install, configure or run Remindr.

Guide
=====

.. toctree::
   :maxdepth: 2

   install
   configure
   use
   license
   authors


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

